# Swedish translation of release-notes for Debian
# Copyright (C) 2006, 2007, 2008, 2009, 2011, 2013 2017, 2019, 2021 Free Software Foundation, Inc.
#
# Martin Bagge <brother@bsnet.se>, 2009, 2011, 2013, 2017, 2019, 2021.
# Martin Ågren <martin.agren@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: release-notes old-stuff.po\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2021-07-27 00:25+0200\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Emacs 27.0.50\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "sv"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Hantera ditt &oldreleasename;-system före uppgraderingen"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Denna bilaga innehåller information om hur du kontrollerar att du kan "
"installera eller uppgradera paket från &oldreleasename; innan du uppgraderar "
"till &releasename;. Det här bör endast vara nödvändigt i specifika "
"situationer."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Uppgradering av ditt &oldreleasename;-system"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Det är inga grundläggande skillnader mot någon annan uppgradering av "
"&oldreleasename; som du gjort. Den enda skillnaden är att du först behöver "
"se till att din paketlista fortfarande innehåller paket från "
"&oldreleasename;, vilket förklaras i <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Om du uppgraderar ditt system med en Debianspegel kommer den automatiskt att "
"uppgraderas till den senaste punktutgåvan av &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Kontrollera dina APT sources.list-filer"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Om någon av raderna i dina APT source.list-filer (se även <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>) "
"refererar till <quote><literal>stable</literal></quote> innebär detta att du "
"redan pekar ut &releasename;. Det kanske inte är vad du vill göra om du inte "
"är redo för uppgraderingen än.  Om du redan har kört <command>apt update</"
"command>, kan du fortfarande komma tillbaka utan problem om du följer "
"nedanstående procedur."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Om du även har installerat paket från &releasename;, är det antagligen inte "
"så stor mening att installera paket från &oldreleasename; längre. I det "
"fallet måste du bestämma dig för om du vill fortsätta eller inte. Det är "
"möjligt att nedgradera paket, men det beskrivs inte här."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Redigera relevanta APT source-list filer, exempelvis <filename>/etc/apt/"
"sources.list</filename> (som root) och kontrollera alla rader som börjar med "
"<literal>deb http:</literal>, <literal>deb https:</literal>, <literal>deb tor"
"+http:</literal>, <literal>deb tor+https:</literal>, <literal>URIs: http:</"
"literal>, <literal>URIs: https:</literal>, <literal>URIs: tor+http:</"
"literal> eller <literal>URIs: tor+https:</literal> efter en referens till "
"<quote><literal>stable</literal></quote>.  Om du hittar någon, ändra "
"<literal>stable</literal> till <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Om du har vissa rader som börjar med <literal>deb file:</literal> eller "
"<literal>URIs: file:</literal> måste du själv kontrollera om platsen som de "
"refererar till innehåller ett arkiv för &oldreleasename; eller &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Ändra inte några rader som börjar med <literal>deb cdrom:</literal> eller "
"<literal>URIs: cdrom:</literal>.  Om du gör det så ogiltigförklaras raden "
"och du måste köra <command>apt-cdrom</command> igen.  Bli inte rädd om en "
"<literal>cdrom</literal>-källrad refererar till <quote><literal>unstable</"
"literal></quote>.  Även om det är förvirrande så är det normalt."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Om du har gjort några ändringar, spara filen och kör"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "för att uppdatera paketlistan."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Ta bort oanvända inställningsfiler"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Innan uppgradering av systemet till &releasename; rekomenderas att radera "
"gamla inställningsfiler (som <filename>*.dpkg-{new,old}</filename>-filer i "
"<filename>/etc</filename>) från systemet."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Uppgradera äldre lokalinställningar till UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "Användning av en lokalanpassning som inte baseras på UTF-8 har inte haft "
#~ "stöd av skrivbordsmiljöer och större mjukvaruprojekt på många år. Sådana "
#~ "lokalanpassningar bör uppgraderas genom att köra <command>dpkg-"
#~ "reconfigure locales</command> och välj en UTF-8-anpassning som standard. "
#~ "Du bör dessutom se till så att användare inte överlagrar "
#~ "standardanpassningen och använder en äldre lokalanpassning i deras miljö."

#~ msgid ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian will remove FTP access to all of its official mirrors on "
#~ "2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
#~ "literal> host, please consider switching to <ulink url=\"https://deb."
#~ "debian.org\">deb.debian.org</ulink>.  This note only applies to mirrors "
#~ "hosted by Debian itself.  If you use a secondary mirror or a third-party "
#~ "repository, then they may still support FTP access after that date.  "
#~ "Please consult with the operators of these if you are in doubt."
#~ msgstr ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian slutar med FTP för alla officiella speglar 2017-11-01</ulink> "
#~ "(länk på engelska). Om din sources.list innehåller en <literal>debian."
#~ "org</literal>-värd bör du överväga att byta till <ulink url=\"https://deb."
#~ "debian.org\">deb.debian.org</ulink>. Detta rör endast speglar som Debian "
#~ "själv tillhandahåller. Använder du en sekundär spegel eller ett "
#~ "tredjeplats-förråd så kan det fortfarande vara så att de tillåter FTP-"
#~ "anslutningar efter detta datum. Hör av dig till respektive leverantör om "
#~ "du är tveksam."

#~ msgid ""
#~ "Lines in sources.list starting with <quote>deb ftp:</quote> and pointing "
#~ "to debian.org addresses should be changed into <quote>deb http:</quote> "
#~ "lines.  See <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."
#~ msgstr ""
#~ "Rader i sources.list som börjar med <quote>deb ftp:</quote> och pekar på "
#~ "adresser i debian.org-domänen ska ändras till <quote>deb http:</quote>-"
#~ "rader. Läs även <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "Att använda lösenord med tecken som inte ingår i ASCII-uppsättningen i "
#~ "samklang med GNOMEs skärmsläckare, pam_ldap eller till och med upplåsning "
#~ "av skärmen är opålitligt om inte UTF-8 används. GNOMEs skärmsläckare "
#~ "drabbas av problemet som beskrivs i felrapport <ulink url=\"http://bugs."
#~ "debian.org/599197\">#599197</ulink>.Filhanteraren Nautilus (och alla glib-"
#~ "baserade program, och troligen även alla Qt-basrade program) antar att "
#~ "filnamn är lagrade i UTF-8. Skalet utgår dock ifrån att filnamnen lagras "
#~ "i aktuell teckenkodning för lokalinställningen. Vid vardaglig användning "
#~ "är filnamn med tecken som inte ingår i ASCII-uppsättningen inte "
#~ "användbara i en sådan installation. Dessutom kräver skärmläsaren gnome-"
#~ "orca (som ger användare med synnedsättning tillgång till GNOMEs "
#~ "skrivbordsmiljö) en UTF-8 lokalanpassning under en gammal "
#~ "teckenuppsättning sedan Squeeze, den kan inte läsa fönsterinformationen "
#~ "från skrivbordselement som Nautilus/GNOME Panel eller Alt-F1-menyn."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Om ditt system är lokaliserat och använder en lokalinställning som inte "
#~ "är baserad på UTF-8 bör du överväga att konvertera ditt system till att "
#~ "använda UTF-8-baserade lokalinställningar. Förr har det förekommit "
#~ "fel<placeholder type=\"footnote\" id=\"0\"/>  som endast kunnat härledas "
#~ "till lokalinställningar baserade på annat än UTF-8. På skrivborden har "
#~ "sådana äldre lokalinställningar endast kunnat stödjas genom fula hack "
#~ "internt i biblioteken och vi kan därför inte på ett korrekt sätt "
#~ "tillhandahålla support för användare som använder dessa."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "För att välja systemets lokalinställningar kan du köra <command>dpkg-"
#~ "reconfigure locales</command>. Säkerställ att du väljer en UTF-8-baserad "
#~ "lokalisering när frågan om vilken lokalinställning som ska användas som "
#~ "standard i systemet visas. Utöver detta bör du kontrollera dina "
#~ "användares lokalinställningar för att säkerställa att de inte har äldre "
#~ "lokaldefinitioner i sina användarmiljöer."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "I och med utgåva 2:1.7.7-12 använder inte xorg-xserver längre filen "
#~ "XF86Config-4. Läs även felrapport <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
